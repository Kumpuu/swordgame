slot_player_swordgame_lvl					= 40
slot_player_swordgame_lvl_kills				= 41
slot_player_swordgame_kills_previous		= 42
slot_player_swordgame_spawn					= 43
slot_player_swordgame_spawn_x				= 44
slot_player_swordgame_spawn_y				= 45
slot_player_swordgame_spawn_z				= 46
slot_player_swordgame_warned				= 47
slot_player_swordgame_spawn_hp				= 48
slot_player_swordgame_spawned				= 49
slot_player_swordgame_death_count			= 50
slot_player_swordgame_remaining_hp_boost	= 51

swordgame_server_message_count			= 3
swordgame_delay_after_win_winner_stage	= 5
swordgame_delay_after_win_next_map		= 30

swordgame_kill_steal_weapon_set_flag		= 0x01
swordgame_kills_per_level_set_flag			= 0x02
swordgame_hp_boost_deaths_set_flag			= 0x04
swordgame_hp_boost_set_flag 				= 0x08
swordgame_hp_boost_heal_interval_set_flag	= 0x10
swordgame_random_lvl_list_order_set_flag	= 0x20

swordgame_winner_stage_x	= 10000
swordgame_winner_stage_y	= 10000
swordgame_winner_stage_z	= 15000