from header_common import *
from header_operations import *
from header_triggers import *
from header_items import *
from module_constants import *
from swordgame_constants import *

swordgame_triggers = [   

	(0, 0, ti_once, 
	[
		(multiplayer_is_server),
		(neq, "$swordgame_gvars_initialized", 1),
	],
	[
		(assign, "$swordgame_gvars_initialized", 1),

		(array_load_file, "$swordgame_winner_stage_data", "@swordgameWinnerStageData"),

		(store_and, ":ksw", "$swordgame_gvar_set_flags", swordgame_kill_steal_weapon_set_flag),
		(store_and, ":kl", "$swordgame_gvar_set_flags", swordgame_kills_per_level_set_flag),
		(store_and, ":hbd", "$swordgame_gvar_set_flags", swordgame_hp_boost_deaths_set_flag),
		(store_and, ":hb", "$swordgame_gvar_set_flags", swordgame_hp_boost_set_flag),
		(store_and, ":hbh", "$swordgame_gvar_set_flags", swordgame_hp_boost_heal_interval_set_flag),
		(store_and, ":llo", "$swordgame_gvar_set_flags", swordgame_random_lvl_list_order_set_flag),

		(try_begin),
			(eq, ":ksw", 0),
			(assign, "$swordgame_kill_steal_weapon", itm_dagger),
		(try_end),
		(try_begin),
			(eq, ":kl", 0),
			(assign, "$swordgame_kills_per_level", 2),
		(try_end),
		(try_begin),
			(eq, ":hbd", 0),
			(assign, "$swordgame_hp_boost_deaths", 6),
		(try_end),
		(try_begin),
			(eq, ":hb", 0),
			(assign, "$swordgame_hp_boost", 250),
		(try_end),
		(try_begin),
			(eq, ":hbh", 0),
			(assign, "$swordgame_hp_boost_heal_interval", 3),
		(try_end),
		(try_begin),
			(eq, ":llo", 0),
			(assign, "$swordgame_random_lvl_list_order", 0),
		(try_end),
	]),


	(ti_before_mission_start, 0, 0,
	[
		(multiplayer_is_server),
		(assign, "$swordgame_server_message", 0),
		(assign, "$swordgame_winner_player_no", -1),

		(array_get_dim_size, ":n", "$swordgame_lvl_lists_ids", 0),
		(gt, ":n", 1),

		(try_begin),
			(eq, "$swordgame_random_lvl_list_order", 1),
			(store_random_in_range, ":next_list", 0, ":n"),

		(else_try),
			(call_script, "script_swordgame_get_cur_lvl_list_no_reg0"),
			(assign, ":next_list", reg0),

			(val_add, ":next_list", 1),

			(ge, ":next_list", ":n"),
			(assign, ":next_list", 0),
		(try_end),

		(call_script, "script_swordgame_set_cur_lvl_list_no", ":next_list"),
	],
	[]),

	(0, swordgame_delay_after_win_next_map, swordgame_delay_after_win_next_map + 1, #WIN TRIGGER
	[
		(multiplayer_is_server),
		(gt, "$swordgame_winner_player_no", -1),

		(str_store_player_username, s0, "$swordgame_winner_player_no"),
		(assign, reg0, swordgame_delay_after_win_next_map),
		  
		(get_max_players, ":num_players"),
		(try_for_range, ":cur_player", 0, ":num_players"),
			(player_is_active, ":cur_player"),
			(multiplayer_send_string_to_player, ":cur_player", multiplayer_event_show_server_message, "@###################################"),
			(multiplayer_send_string_to_player, ":cur_player", multiplayer_event_show_server_message, "@# {s0} won the SwordGame!"),
			(multiplayer_send_string_to_player, ":cur_player", multiplayer_event_show_server_message, "@###################################"),
			(multiplayer_send_string_to_player, ":cur_player", multiplayer_event_show_server_message, "@Next map in {reg0}s"),
		(try_end),

		(multiplayer_send_string_to_player, "$swordgame_winner_player_no", multiplayer_event_show_server_message , "@////////////////////////////////////////////"),
		(multiplayer_send_string_to_player, "$swordgame_winner_player_no", multiplayer_event_show_server_message , "@//  Congratulations! You are the winner!  //"),
		(multiplayer_send_string_to_player, "$swordgame_winner_player_no", multiplayer_event_show_server_message , "@////////////////////////////////////////////"),

		(player_get_agent_id, ":winner_agent_no", "$swordgame_winner_player_no"),
		(agent_is_active,":winner_agent_no"),
		(agent_is_alive,":winner_agent_no"),
		(agent_equip_item, ":winner_agent_no", itm_warhammer),	 
	   	(agent_set_wielded_item, ":winner_agent_no", itm_warhammer),
	],
	[
		(assign, "$swordgame_winner_player_no", -1),

		(call_script, "script_game_multiplayer_get_game_type_mission_template", "$g_multiplayer_game_type"),
		(start_multiplayer_mission, reg0, "$g_multiplayer_selected_map", 0),
		(call_script, "script_game_set_multiplayer_mission_end"), 
	]),

	(0, swordgame_delay_after_win_winner_stage, swordgame_delay_after_win_next_map + 1, #WINNER STAGE TRIGGER
	[
		(multiplayer_is_server),
		(gt, "$swordgame_winner_player_no", -1),
	],
	[
		(call_script, "script_swordgame_setup_win_stage"),
	]),

	(0.1, 0, 2, #make the winner pretty and yell!
	[
		(multiplayer_is_server),
		(gt, "$swordgame_winner_player_no", -1),

		(player_is_active, "$swordgame_winner_player_no"),
		(player_get_agent_id, ":winner_agent_no", "$swordgame_winner_player_no"),
		(agent_is_active,":winner_agent_no"),
		(agent_is_alive,":winner_agent_no"),
	],
	[
		(player_get_agent_id, ":winner_agent_no", "$swordgame_winner_player_no"),

		(try_begin),
			(call_script, "script_cf_swordgame_agent_is_male", ":winner_agent_no"),

			(store_random_in_range, ":rand", 0, 3),
			(try_begin),
				(eq, ":rand", 0),
				(assign, ":snd", "snd_man_yell"),
			(else_try),
				(eq, ":rand", 1),
				(assign, ":snd", "snd_man_victory"),
			(else_try),
				(eq, ":rand", 2),
				(assign, ":snd", "snd_encounter_vaegirs_ally"),
			(try_end),
        (else_try),
          (assign, ":snd", "snd_woman_yell"),
        (try_end),

		(agent_play_sound, ":winner_agent_no", ":snd"),

		(agent_get_position, pos0, ":winner_agent_no"),
		(position_move_z, pos0, 400),

		(particle_system_burst, "psys_wedding_rose", pos0, 30),
	]),

	(ti_on_agent_spawn, 0, 0, 
	[
		(multiplayer_is_server),
		(gt, "$swordgame_winner_player_no", -1),
	],
	[
		(store_trigger_param_1, ":agent_no"),

		(agent_is_human, ":agent_no"),
		(try_begin),
			(neg|agent_is_non_player, ":agent_no"),
			(agent_get_player_id, ":player_no", ":agent_no"),

			(eq, ":player_no", "$swordgame_winner_player_no"),
			(agent_set_no_death_knock_down_only, ":agent_no", 1),

			(call_script, "script_swordgame_get_winner_position_pos0"),
			(agent_set_position, ":agent_no", pos0),

		(else_try),
			(call_script, "script_swordgame_agent_remove_all_items", ":agent_no"),
			(agent_set_speed_modifier, ":agent_no", 50),

			(call_script, "script_swordgame_get_random_loser_position_pos0"),
			(agent_set_position, ":agent_no", pos0),
		(try_end),
	]),

	(ti_on_agent_hit, 0, 0, #no damage for losers
	[
		(multiplayer_is_server),
		(gt, "$swordgame_winner_player_no", -1),
	],
	[
		(store_trigger_param_2, ":dealer"),

		(agent_is_human, ":dealer"),
		
		(try_begin),
			(neg|agent_is_non_player, ":dealer"),
			(agent_get_player_id, ":player_no", ":dealer"),

			(eq, ":player_no", "$swordgame_winner_player_no"),
			#do nothing
		(else_try),
			(set_trigger_result, 0),
		(try_end),
	]),

	(ti_server_player_joined, 0, 0, 
	[
		(multiplayer_is_server),
	],
	[
		(store_trigger_param_1, ":player_no"),

		(player_set_slot, ":player_no", slot_player_swordgame_kills_previous, -1),

		(call_script, "script_swordgame_get_cur_lvl_list_name_s0"),
		(multiplayer_send_string_to_player, ":player_no", multiplayer_event_show_server_message, "@Current weapon list: {s0}"),
	]),
	

	(0.1, 0, 0, #respawn after lvl up
	[
		(multiplayer_is_server),
	],
	[
	 (get_max_players, ":num_players"),
	 (try_for_range, ":player_no", 0, ":num_players"),
	   (player_is_active, ":player_no"),
	   (player_get_agent_id, ":agent_no", ":player_no"),
	   (ge, ":agent_no", 0),	 
	 
	   (player_slot_eq, ":player_no", slot_player_swordgame_spawn, 1),
	   
	   (player_set_slot, ":player_no", slot_player_swordgame_spawn, 0),	
	 
	   (player_get_slot, ":x", ":player_no", slot_player_swordgame_spawn_x),
	   (player_get_slot, ":y", ":player_no", slot_player_swordgame_spawn_y),
	   (player_get_slot, ":z", ":player_no", slot_player_swordgame_spawn_z),
	   (player_get_slot, ":hp", ":player_no", slot_player_swordgame_spawn_hp),
	 
	   (position_set_x, pos0, ":x"),
	   (position_set_y, pos0, ":y"),
	   (position_set_z, pos0, ":z"),
	   (agent_set_position, ":agent_no", pos0),
	   (agent_set_hit_points, ":agent_no", ":hp"),
	 (try_end),
	]),

	(10, 0, 0, #give players the right weapon
	[
		(multiplayer_is_server),
		(eq, "$swordgame_winner_player_no", -1),
	],
	[
	 (get_max_players, ":num_players"),
	 (try_for_range, ":player_no", 0, ":num_players"),
	   (player_is_active, ":player_no"),
	   
	   (player_get_agent_id, ":agent_no", ":player_no"),
	   (ge, ":agent_no", 0),
	   (agent_is_human, ":agent_no"),
	   (agent_is_alive, ":agent_no"),
	   (neg|agent_is_non_player, ":agent_no"),	   
	 
	   (agent_get_wielded_item, ":wpn", ":agent_no", 0),
	   (player_get_slot, ":lvl", ":player_no", slot_player_swordgame_lvl),
	   
	   (array_get_val, ":correct_wpn", "$swordgame_cur_lvl_list", ":lvl", 0),
	   (neq, ":wpn", ":correct_wpn"),
	   (neq, ":wpn", "$swordgame_kill_steal_weapon"),

	   (gt, ":correct_wpn", 0),
	   (agent_equip_item, ":agent_no", ":correct_wpn"),	 
	   (agent_set_wielded_item, ":agent_no", ":correct_wpn"),
	 (try_end),
	 ]),

	(90, 0, 0, #server messages
	[
		(multiplayer_is_server),
	],
	[
		(try_begin),
		  (eq, "$swordgame_server_message", 0),
		  (str_store_string, s0, "@SwordGame by AgentSmith"),
		(else_try),
		  (eq, "$swordgame_server_message", 1),
		  (str_store_string, s0, "@Feedback to kumpuu@web.de"),
		(else_try),
		  (eq, "$swordgame_server_message", 2),
		  (str_store_string, s0, "@Idea and support: AgentFirz, AgentNeo and AgentHans"),
		(try_end),

		(val_add,  "$swordgame_server_message", 1),
		(try_begin),
		  (ge, "$swordgame_server_message", swordgame_server_message_count),
		  (assign, "$swordgame_server_message", 0),
		(try_end),		  

		(get_max_players, ":num_players"),
		(try_for_range, ":cur_player", 0, ":num_players"),
		  (player_is_active, ":cur_player"),
		  (multiplayer_send_string_to_player, ":cur_player", multiplayer_event_show_server_message, "@{s0}"),
		(try_end),  
	]),

	(ti_on_agent_killed_or_wounded, 0, 0, #lvl up & death count
	[
		(multiplayer_is_server),
		(eq, "$swordgame_winner_player_no", -1),
	],
	[
		(store_trigger_param, ":dead_agent_no", 1), 
		(store_trigger_param, ":killer_agent_no", 2),

		(agent_is_human, ":dead_agent_no"),
		#(agent_set_wielded_item, ":dead_agent_no", 0),
		(call_script, "script_swordgame_player_lvl_up", ":killer_agent_no", ":dead_agent_no"),
		(call_script, "script_swordgame_agent_remove_all_items", ":dead_agent_no"),

		(neg|agent_is_non_player, ":dead_agent_no"),

		(agent_get_player_id, ":dead_player_no", ":dead_agent_no"),
		(ge, ":dead_player_no", 0),

		(player_get_slot, ":death_count", ":dead_player_no", slot_player_swordgame_death_count),
		(val_add, ":death_count", 1),				 
		(player_set_slot, ":dead_player_no", slot_player_swordgame_death_count, ":death_count"),

		(ge, ":death_count", "$swordgame_hp_boost_deaths"),
		(gt, "$swordgame_hp_boost", 0),
		(player_set_slot, ":dead_player_no", slot_player_swordgame_remaining_hp_boost, "$swordgame_hp_boost"),

		(assign, reg0, "$swordgame_hp_boost_deaths"),
		(assign, reg1, "$swordgame_hp_boost"),
		(multiplayer_send_string_to_player, ":dead_player_no", multiplayer_event_show_server_message, "@{reg0} deaths in a row - you get a hp boost of {reg1}"),
	]),
	  
	#heal player with hp boost
	(0.1, 0, 0,
	 [
		(multiplayer_is_server),
	 ],
	 [ 
	  (try_for_agents, ":agent_no"),
	    (agent_is_human, ":agent_no"),
	    (agent_is_alive, ":agent_no"),
	    (neg|agent_is_non_player, ":agent_no"),
	  
	    (store_agent_hit_points, ":hp", ":agent_no"),
		(lt, ":hp", 100),
	  
	    (agent_get_player_id, ":player_no", ":agent_no"),
	    (ge, ":player_no", 0),
	    (player_is_active, ":player_no"),
	 
	    #(player_get_slot, ":death_count", ":player_no", slot_player_swordgame_death_count),
	    #(ge, ":death_count", swordgame_hp_boost_deaths),
		
	    (player_get_slot, ":remaining_hp_boost",  ":player_no", slot_player_swordgame_remaining_hp_boost),
	    (gt, ":remaining_hp_boost", 0),		    
		
	    (assign, ":i", 100),
	    (val_sub, ":i", ":hp"),
		(val_min, ":i", "$swordgame_hp_boost_heal_interval"),
	  	
	    (try_begin),
	  	  (gt, ":i", ":remaining_hp_boost"),
		  (val_add, ":hp", ":remaining_hp_boost"),
		  (assign, ":remaining_hp_boost", 0),			  
		(else_try),
	      (val_add, ":hp", ":i"),
		  (val_sub, ":remaining_hp_boost", ":i"),		  
		(try_end),	

	    (player_set_slot, ":player_no", slot_player_swordgame_remaining_hp_boost, ":remaining_hp_boost"),
	    (agent_set_hit_points, ":agent_no", ":hp"),
	  (try_end),
	]),	

]		