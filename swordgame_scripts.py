from header_common import *
from header_operations import *
from header_items import *
from ID_animations import *
from module_constants import *
from swordgame_constants import *

swordgame_scripts = [
  #script_swordgame_player_lvl_up
  ("swordgame_player_lvl_up",
    [
	  (store_script_param, ":agent_no", 1),
	  (store_script_param, ":dead_agent_no", 2),
	  
	  (try_begin),
	    (ge, ":agent_no", 0),
		(ge, ":dead_agent_no", 0),
	    (agent_is_human, ":dead_agent_no"), #no lvl up for horse kills
	    (agent_is_alive, ":agent_no"),
		(neg|agent_is_non_player, ":agent_no"),
		
		(agent_get_player_id, ":player_no", ":agent_no"),
		(neq, ":player_no", -1),
		(player_is_active, ":player_no"),
		
		(player_get_kill_count, ":kills", ":player_no"),
		(player_get_slot, ":kills_previous", ":player_no",  slot_player_swordgame_kills_previous),
		
		(player_set_slot, ":player_no", slot_player_swordgame_kills_previous, ":kills"),
		(gt, ":kills", ":kills_previous"), #killcheck

		(player_set_slot, ":player_no", slot_player_swordgame_death_count, 0), #reset death count
		
		(agent_get_wielded_item, ":wpn", ":agent_no", 0), 
		(player_get_slot, ":lvl", ":player_no", slot_player_swordgame_lvl),
		
		(assign, ":continue", 0),
		(try_begin), #kill with the right weapon?
			(array_get_val, ":lvl_wpn", "$swordgame_cur_lvl_list", ":lvl", 0),

			(try_begin),
				(item_has_property, ":lvl_wpn", itp_next_item_as_melee),
				(store_add, ":lvl_alt_wpn", ":lvl_wpn", 1),
			(else_try),
				(assign, ":lvl_alt_wpn", -1),
			(try_end),

			(this_or_next|eq, ":wpn", ":lvl_wpn"),
			(eq, ":wpn", ":lvl_alt_wpn"),

			(player_set_slot, ":player_no", slot_player_swordgame_warned, 0),	
			(assign, ":continue", 1),
		  
        (else_try),  #lvl steal
          (eq, ":wpn", "$swordgame_kill_steal_weapon"),

		  (try_begin),
		  	(agent_is_non_player, ":dead_agent_no"),
		  	(multiplayer_send_string_to_player, ":player_no", multiplayer_event_show_server_message , "@You can't steal levels from bots."),

		  (else_try),
			  (agent_get_player_id, ":dead_player_no", ":dead_agent_no"),
			  (ge, ":dead_player_no", 0),
			  
			  (player_get_slot, ":dead_player_lvl", ":dead_player_no", slot_player_swordgame_lvl),
			  (ge, ":dead_player_lvl", ":lvl"),
			  
			  (player_set_slot, ":player_no", slot_player_swordgame_lvl_kills, "$swordgame_kills_per_level"),
			  (val_sub, ":dead_player_lvl", 1),
			  (val_max, ":dead_player_lvl", 0),

			  (player_set_slot, ":dead_player_no", slot_player_swordgame_lvl, ":dead_player_lvl"),
			  (player_set_slot, ":dead_player_no", slot_player_swordgame_lvl_kills, 0),
			  (call_script, "script_swordgame_player_set_equipment", ":dead_player_no"),
			  
			  (str_store_player_username, s0, ":player_no"),
			  (str_store_player_username, s1, ":dead_player_no"),
			  (get_max_players, ":num_players"),
			  (try_for_range, ":cur_player", 0, ":num_players"),
			    (player_is_active, ":cur_player"),
	            (multiplayer_send_string_to_player, ":cur_player", multiplayer_event_show_server_message, "@{s0} stole a level from {s1}!"),
	          (try_end),
			  (assign, ":continue", 1),
		  (try_end),
		  
		(else_try),
		  (player_get_slot, ":warned", ":player_no", slot_player_swordgame_warned),
		  (try_begin),
		    (eq, ":warned", 0),
		    (multiplayer_send_string_to_player, ":player_no", multiplayer_event_show_server_message , "@You ONLY level up with the weapon you get !!"),
			(player_set_slot, ":player_no", slot_player_swordgame_warned, 1),	
		  (else_try),
		    (multiplayer_send_string_to_player, ":player_no", multiplayer_event_show_server_message , "@---------------------------------------------------"),
			(multiplayer_send_string_to_player, ":player_no", multiplayer_event_show_server_message , "@You ONLY level up with the weapon you get !!"),
			(multiplayer_send_string_to_player, ":player_no", multiplayer_event_show_server_message , "@---------------------------------------------------"),
		  (try_end),
        (try_end),
       
 	    (eq, ":continue", 1), 		
		
		(player_get_slot, ":lvl_kills", ":player_no",  slot_player_swordgame_lvl_kills),
		(val_add, ":lvl_kills", 1),
		(player_set_slot, ":player_no", slot_player_swordgame_lvl_kills, ":lvl_kills"),

		(try_begin),
			(gt, "$swordgame_kills_per_level", 1),

			(assign, reg0, ":lvl_kills"),
			(assign, reg1, "$swordgame_kills_per_level"),
			(multiplayer_send_string_to_player, ":player_no", multiplayer_event_show_server_message , "@{reg0}/{reg1}"),
		(try_end),
		
		(ge, ":lvl_kills", "$swordgame_kills_per_level"),
		(player_set_slot, ":player_no", slot_player_swordgame_lvl_kills, 0),
		
		(val_add, ":lvl", 1), ### LVL UP!! ###
		(player_set_slot, ":player_no", slot_player_swordgame_lvl, ":lvl"),

		(array_get_dim_size, ":max_lvl", "$swordgame_cur_lvl_list", 0),
		(val_sub, ":max_lvl", 1),

        (try_begin),
		  (gt, ":lvl", ":max_lvl"), #GAME OVER!

		  (agent_set_no_death_knock_down_only, ":agent_no", 1),
		  (agent_play_sound, ":agent_no", "snd_quest_succeeded"),
		  (assign, "$swordgame_winner_player_no", ":player_no"),
 
		(else_try),	
			(agent_get_position, pos1, ":agent_no"), #RESPAWN

			(store_agent_hit_points, ":hp", ":agent_no"),		  
			(position_get_x, ":x", pos1),
			(position_get_y, ":y", pos1),
			(position_get_z, ":z", pos1),
			(player_set_slot, ":player_no", slot_player_swordgame_spawn_x, ":x"),
			(player_set_slot, ":player_no", slot_player_swordgame_spawn_y, ":y"),
			(player_set_slot, ":player_no", slot_player_swordgame_spawn_z, ":z"),
			(player_set_slot, ":player_no", slot_player_swordgame_spawn_hp, ":hp"),	

			(position_move_z, pos1, 200),
			(particle_system_burst, "psys_fire_sparks_1", pos1, 35),

			(agent_fade_out, ":agent_no"), #respawn with new equip
			(call_script, "script_swordgame_player_set_equipment", ":player_no"),		
			(player_spawn_new_agent, ":player_no", 0),
			(player_set_slot, ":player_no", slot_player_swordgame_spawn, 1),			

			(assign, reg0, ":lvl"),
			(val_add, reg0, 1),

			(assign, reg1, ":max_lvl"),
			(val_add, reg1, 1),

			(multiplayer_send_string_to_player, ":player_no", multiplayer_event_show_server_message , "@You reached level {reg0}/{reg1}"),

			(str_store_player_username, s0, ":player_no"),
			(get_max_players, ":num_players"),
			(try_for_range, ":cur_player", 0, ":num_players"),
				(neq, ":cur_player", ":player_no"),
				(player_is_active, ":cur_player"),
				(multiplayer_send_string_to_player, ":cur_player", multiplayer_event_show_server_message, "@{s0} reached level {reg0}!"),
			(try_end),
		(try_end),

		(try_begin),
		  (eq, ":lvl", ":max_lvl"),
		  (multiplayer_send_string_to_player, ":player_no", multiplayer_event_show_server_message, "@This is the last level! Come on!"),
		  (get_max_players, ":num_players"),
		  (str_store_player_username, s0, ":player_no"),

		  (try_for_range, ":cur_player", 0, ":num_players"),
            (neq, ":cur_player", ":player_no"),
		    (player_is_active, ":cur_player"),
            (multiplayer_send_string_to_player, ":cur_player", multiplayer_event_show_server_message, "@{s0} is on the last level and will win soon! Stop him!"),
          (try_end),
		(try_end),	
	  (try_end),
	]),
  
  #script_swordgame_player_set_equipment
  ("swordgame_player_set_equipment",
    [
     (store_script_param, ":player_no", 1),	 
	  
	 (try_begin),
		(gt, ":player_no", 0),
		(player_is_active, ":player_no"), 
			  
		(try_begin),
			(player_slot_eq, ":player_no", slot_player_swordgame_spawned, 0),
			(player_set_slot, ":player_no", slot_player_swordgame_spawned, 1),

			(assign, ":min_level", -1),

			(try_for_players, ":cur_player", 1),
			  (neq, ":cur_player", ":player_no"),
			  (player_is_active, ":cur_player"),
			  
			  (player_get_slot, ":cur_player_spawned", ":cur_player", slot_player_swordgame_spawned),
			  (neq, ":cur_player_spawned", 0),
			  
			  (player_get_slot, ":lvl", ":cur_player", slot_player_swordgame_lvl),
			  
			  (this_or_next|lt, ":lvl", ":min_level"),
			  (eq, ":min_level", -1),

			  (assign, ":min_level", ":lvl"),	  
			(try_end),

			(val_max, ":min_level", 0),	
			(player_set_slot, ":player_no", slot_player_swordgame_lvl, ":min_level"),	
		(try_end),

				  
		(player_get_slot, ":lvl", ":player_no", slot_player_swordgame_lvl),

		(assign, ":free_item_index", -1),
		(try_for_range, ":cur_index", 0, 9),
			(array_get_val, ":item_no", "$swordgame_cur_lvl_list", ":lvl", ":cur_index"),

			(try_begin),
				(gt, ":item_no", 0),
				(player_add_spawn_item, ":player_no", ":cur_index", ":item_no"),
			(else_try),
				(eq, ":free_item_index", -1),
				(le, ":cur_index", ek_item_3),
				(assign, ":free_item_index", ":cur_index"),
			(try_end),
		(try_end),

		(try_begin),
			(neq, "$swordgame_kill_steal_weapon", -1),
			(neq, ":free_item_index", -1),

			(player_add_spawn_item, ":player_no", ":free_item_index", "$swordgame_kill_steal_weapon"),
		(try_end),
	 (try_end),
    ]),

  	#script_cf_swordgame_get_item_no_reg0_from_s0
  	#input: s0 - name
  	#output: reg0 - item no (-1 if not found)
  	("cf_swordgame_get_item_no_reg0_from_s0", [
  		(try_begin),
			(str_is_integer, s0),
			(str_to_num, reg0, s0),

		(else_try),

	  		(assign, reg0, itm_items_end),
	  		(assign, ":end", itm_items_end),
	  		(try_for_range, ":cur_item_no", 0, ":end"),
				(str_store_item_id, s1, ":cur_item_no"),
				
				(str_equals, s0, s1, 1),
				(assign, reg0, ":cur_item_no"),
				(assign, ":end", itm_items_end), #ends loop
			(try_end),
		(try_end),

		(ge, reg0, 0),
		(lt, reg0, itm_items_end),
  	]),

  	#script_swordgame_add_item_to_array
	("swordgame_add_item_to_array", [
		(store_script_param, ":item_arr", 1),
		(store_script_param, ":item_no", 2),

		(item_get_type, ":item_type", ":item_no"),

		(assign, ":index", -1),

		(try_begin),
			(assign, ":cont", 0),
			(try_begin),
				(ge, ":item_type", itp_type_one_handed_wpn),
				(le, ":item_type", itp_type_thrown),
				(assign, ":cont", 1),
			(else_try),
				(ge, ":item_type", itp_type_pistol),
				(le, ":item_type", itp_type_bullets),
				(assign, ":cont", 1),
			(try_end),

			(eq, ":cont", 1),

			(assign, ":end", 4),
			(try_for_range, ":i", 0, ":end"),
				(array_get_val, ":cur_val", ":item_arr", ":i"),

				(eq, ":cur_val", 0),

				(assign, ":index", ":i"),
				(assign, ":end", 0), #break loop
			(try_end),

		(else_try),
			(ge, ":item_type", itp_type_head_armor),
			(le, ":item_type", itp_type_hand_armor),

			(store_sub, ":index", ":item_type", itp_type_head_armor), #-> 0-3
			(val_add, ":index", ek_head),

		(else_try),
			(eq, ":item_type", itp_type_horse),
			(assign, ":index", ek_horse),
		(try_end),

		(try_begin),
			(ge, ":index", 0),
			(array_set_val, ":item_arr", ":item_no", ":index"),
		(else_try),
			(assign, reg0, ":item_no"),
			(display_message, "@unable to add item_no {reg0}"),
		(try_end),

		##### array layout #####
			#ek_item_0 = 0
			#ek_item_1 = 1
			#ek_item_2 = 2
			#ek_item_3 = 3

			#ek_head   = 4
			#ek_body   = 5
			#ek_foot   = 6
			#ek_gloves = 7

			#ek_horse  = 8
	]),

	#script_swordgame_add_new_lvl_list
	#input: s0 - list name
	("swordgame_add_new_lvl_list", [
		(try_begin),
  			(eq, "$swordgame_lvl_lists_initialized", 0),

  			(array_create, "$swordgame_lvl_lists_ids", 0, 0),
  			(array_create, "$swordgame_lvl_lists_names", 1, 0),

			(assign, "$swordgame_lvl_lists_initialized", 1),
		(try_end),

		(array_create, "$swordgame_cur_lvl_list", 0, 0, 9),

		(array_push, "$swordgame_lvl_lists_ids", "$swordgame_cur_lvl_list"),
		(array_push, "$swordgame_lvl_lists_names", s0),
	]),

	#script_swordgame_set_cur_lvl_list_no
	("swordgame_set_cur_lvl_list_no", [
		(store_script_param, ":list_no", 1),

		(array_get_val, "$swordgame_cur_lvl_list", "$swordgame_lvl_lists_ids", ":list_no"),
	]),

	#script_swordgame_get_cur_lvl_list_no_reg0
	("swordgame_get_cur_lvl_list_no_reg0", [
		(call_script, "script_swordgame_get_lvl_list_no_reg0", "$swordgame_cur_lvl_list"),
	]),

	#script_swordgame_get_lvl_list_no_reg0
	("swordgame_get_lvl_list_no_reg0", [
		(store_script_param, ":list_id", 1),

		(array_get_dim_size, ":n", "$swordgame_lvl_lists_ids", 0),

		(assign, reg0, -1),
		(try_for_range, ":i", 0, ":n"),
			(array_eq, "$swordgame_lvl_lists_ids", ":list_id", ":i"),

			(assign, reg0, ":i"),
			(assign, ":n", 0),
		(try_end),
	]),

	#script_swordgame_get_cur_lvl_list_name_s0
	("swordgame_get_cur_lvl_list_name_s0", [
		(call_script, "script_swordgame_get_cur_lvl_list_no_reg0"),

		(array_get_val, s0, "$swordgame_lvl_lists_names", reg0),
	]),

  	#script_swordgame_add_items_from_str_to_list
  	#input: s0 - items str
  	("swordgame_add_items_from_str_to_list", [
		(str_split, ":num_items", s1, s0, "@,", 1),

		(array_create, ":item_arr", 0, 9),
		(assign, ":items_added", 0),

		(try_for_range, ":cur", 0, ":num_items"),
			(store_add, ":cur_str", ":cur", 1),
			(str_store_string_reg, s0, ":cur_str"),
			(str_store_trim, s0, s0),

			(try_begin),
				(call_script, "script_cf_swordgame_get_item_no_reg0_from_s0"),

				(val_add, ":items_added", 1),
				(call_script, "script_swordgame_add_item_to_array", ":item_arr", reg0),
			(else_try),
				(display_message, "@unrecognized item: {s0}"),
			(try_end),
		(try_end),

		(gt, ":items_added", 0),

		(array_push, "$swordgame_cur_lvl_list", ":item_arr"),
		(array_free, ":item_arr"),
  	]),

	#script_swordgame_set_gvar
	("swordgame_set_gvar", [
		(str_split, ":num_args", s1, s0, "@ ", 1),
		(assign, ":suc", 0),

		(try_begin),
			(eq, ":num_args", 2),

			(str_store_string_reg, s0, s1),
			(str_store_replace_underscores_with_spaces, s1, s1),

			(try_begin),
				(str_equals, s1, "@lvl steal wpn"),

				(try_begin),
					(str_equals, s2, "@-1"),
					(assign, "$swordgame_kill_steal_weapon", -1),
					(val_or, "$swordgame_gvar_set_flags", swordgame_kill_steal_weapon_set_flag),
					(assign, ":suc", 1),

				(else_try),
					(str_store_string_reg, s0, s2),
					(call_script, "script_cf_swordgame_get_item_no_reg0_from_s0"),

					(assign, "$swordgame_kill_steal_weapon", reg0),
					(val_or, "$swordgame_gvar_set_flags", swordgame_kill_steal_weapon_set_flag),
					(assign, ":suc", 1),
				(try_end),

			(else_try),
				(str_is_integer, s2),
				(str_to_num, ":val", s2),

				(try_begin),
					(str_equals, s1, "@kills per lvl"),

					(gt, ":val", 0),
					(assign, "$swordgame_kills_per_level", ":val"),
					(val_or, "$swordgame_gvar_set_flags", swordgame_kills_per_level_set_flag),
					(assign, ":suc", 1),

				(else_try),
					(str_equals, s1, "@hp boost"),

					(ge, ":val", 0),
					(assign, "$swordgame_hp_boost", ":val"),
					(val_or, "$swordgame_gvar_set_flags", swordgame_hp_boost_set_flag),
					(assign, ":suc", 1),

				(else_try),
					(str_equals, s1, "@hp boost deaths"),

					(gt, ":val", 0),
					(assign, "$swordgame_hp_boost_deaths", ":val"),
					(val_or, "$swordgame_gvar_set_flags", swordgame_hp_boost_deaths_set_flag),
					(assign, ":suc", 1),

				(else_try),
					(str_equals, s1, "@hp boost heal interval"),

					(gt, ":val", 0),
					(assign, "$swordgame_hp_boost_heal_interval", ":val"),
					(val_or, "$swordgame_gvar_set_flags", swordgame_hp_boost_heal_interval_set_flag),
					(assign, ":suc", 1),

				(else_try),
					(str_equals, s1, "@rand order"),

					(is_between, ":val", 0, 2),
					(assign, "$swordgame_random_lvl_list_order", ":val"),
					(val_or, "$swordgame_gvar_set_flags", swordgame_random_lvl_list_order_set_flag),
					(assign, ":suc", 1),
				(try_end),
			(try_end),
		(try_end),

		(try_begin),
			(neq, ":suc", 1),
			(display_message, "@Unable to set {s0} to {s2}"),
		(try_end),
	]),

	#script_swordgame_agent_remove_all_items
	("swordgame_agent_remove_all_items", [
		(store_script_param, ":agent_no", 1),

		(agent_get_item_slot, ":weapon1", ":agent_no", 0),
		(agent_get_item_slot, ":weapon2", ":agent_no", 1),
		(agent_get_item_slot, ":weapon3", ":agent_no", 2),
		(agent_get_item_slot, ":weapon4", ":agent_no", 3),
		
		(try_begin),
			(ge, ":weapon1", 0),
			(agent_unequip_item, ":agent_no", ":weapon1"),
		(try_end),
		(try_begin),
			(ge, ":weapon2", 0),
			(agent_unequip_item, ":agent_no", ":weapon2"),
		(try_end),
		(try_begin),
			(ge, ":weapon3", 0),
			(agent_unequip_item, ":agent_no", ":weapon3"),
		(try_end),
		(try_begin),
			(ge, ":weapon4", 0),
			(agent_unequip_item, ":agent_no", ":weapon4"),	
		(try_end),
	]),

	#script_cf_swordgame_agent_is_male
	("cf_swordgame_agent_is_male", [
		(store_script_param, ":agent_no", 1),

		(agent_get_troop_id,":trp",":agent_no"),
		(troop_get_type,":gender",":trp"),

		(try_begin),
		  (agent_get_player_id,":pid",":agent_no"),
		  (player_is_active,":pid"),
		  (player_get_gender, ":gender", ":pid"),
		(try_end),

		(eq, ":gender", 0),
	]),

	#script_swordgame_get_random_loser_position_pos0
	("swordgame_get_random_loser_position_pos0",
	[
		(array_get_dim_size, ":n", "$swordgame_winner_stage_data", 0),
		(store_sub, ":loser_rectangle_index_2", ":n", 2),
		(store_sub, ":loser_rectangle_index_1", ":n", 1),

		(array_get_val, ":x2", "$swordgame_winner_stage_data", ":loser_rectangle_index_2", 1),
		(array_get_val, ":y2", "$swordgame_winner_stage_data", ":loser_rectangle_index_2", 2),

		(array_get_val, ":x1", "$swordgame_winner_stage_data", ":loser_rectangle_index_1", 1),
		(array_get_val, ":y1", "$swordgame_winner_stage_data", ":loser_rectangle_index_1", 2),
		(array_get_val, ":z", "$swordgame_winner_stage_data", ":loser_rectangle_index_1", 3),

		(store_sub, ":range_x", ":x2", ":x1"),
		(store_sub, ":range_y", ":y2", ":y2"),

		(store_random_in_range, ":add_x", 0, ":range_x"),
		(store_random_in_range, ":add_y", 0, ":range_y"),

		(init_position, pos0),
		(position_set_x, pos0, swordgame_winner_stage_x),
		(position_set_y, pos0, swordgame_winner_stage_y),

		(position_set_z_to_ground_level, pos0),
		(position_move_z, pos0, swordgame_winner_stage_z),

		(position_move_x, pos0, ":x1"),
		(position_move_y, pos0, ":y1"),
		(position_move_z, pos0, ":z"),

		(position_move_x, pos0, ":add_x"),
		(position_move_y, pos0, ":add_y"),
	]),
	
	#swordgame_get_winner_position_pos0
	("swordgame_get_winner_position_pos0",
	[
		(array_get_dim_size, ":n", "$swordgame_winner_stage_data", 0),
		(store_sub, ":winner_pos_index", ":n", 3),

		(init_position, pos0),
		(position_set_x, pos0, swordgame_winner_stage_x),
		(position_set_y, pos0, swordgame_winner_stage_y),

		(position_set_z_to_ground_level, pos0),
		(position_move_z, pos0, swordgame_winner_stage_z),

		(array_get_val, ":x", "$swordgame_winner_stage_data", ":winner_pos_index", 1),
		(array_get_val, ":y", "$swordgame_winner_stage_data", ":winner_pos_index", 2),
		(array_get_val, ":z", "$swordgame_winner_stage_data", ":winner_pos_index", 3),

		(position_move_x, pos0, ":x"),
		(position_move_y, pos0, ":y"),
		(position_move_z, pos0, ":z"),
	]),

	#script_swordgame_setup_win_stage
	("swordgame_setup_win_stage", [
		(set_fixed_point_multiplier, 100),

		(player_get_agent_id, ":winner_agent_no", "$swordgame_winner_player_no"),

		(array_get_dim_size, ":n", "$swordgame_winner_stage_data", 0),
		(store_sub, ":winner_pos_index", ":n", 3),
		(store_sub, ":loser_rectangle_index_2", ":n", 2),
		(store_sub, ":loser_rectangle_index_1", ":n", 1),

		(init_position, pos0),
		(position_set_x, pos0, swordgame_winner_stage_x),
		(position_set_y, pos0, swordgame_winner_stage_y),

		(position_set_z_to_ground_level, pos0),
		(position_move_z, pos0, swordgame_winner_stage_z),

		(try_for_range, ":i", 0, ":n"),
			(position_copy_origin, pos1, pos0),
			(position_copy_rotation, pos1, pos0),

			(array_get_val, ":id", "$swordgame_winner_stage_data", ":i", 0),

			(array_get_val, ":x", "$swordgame_winner_stage_data", ":i", 1),
			(array_get_val, ":y", "$swordgame_winner_stage_data", ":i", 2),
			(array_get_val, ":z", "$swordgame_winner_stage_data", ":i", 3),

			(array_get_val, ":rx", "$swordgame_winner_stage_data", ":i", 4),
			(array_get_val, ":ry", "$swordgame_winner_stage_data", ":i", 5),
			(array_get_val, ":rz", "$swordgame_winner_stage_data", ":i", 6),

			(position_move_x, pos1, ":x"),
			(position_move_y, pos1, ":y"),
			(position_move_z, pos1, ":z"),

			(position_rotate_x, pos1, ":rx"),
			(position_rotate_y, pos1, ":ry"),
			(position_rotate_z, pos1, ":rz"),

			#(assign, reg0, ":id"),
			#(assign, reg1, ":x"),
			#(display_message, "@id {reg0}, x {reg1}"),

			(try_begin),
				(eq, ":i", ":winner_pos_index"),

				(try_begin),
					(agent_is_active,":winner_agent_no"),
					(agent_is_alive,":winner_agent_no"),

					(agent_set_position, ":winner_agent_no", pos1),

					#(agent_equip_item, ":winner_agent_no", itm_warhammer),	 
		   			#(agent_set_wielded_item, ":winner_agent_no", itm_warhammer),

		   			(agent_set_animation, ":winner_agent_no", anim_cheer, 1),

		   			(player_get_gender, ":gender", "$swordgame_winner_player_no"),

					(assign, ":snd", "snd_man_victory"),
					(try_begin),
						(eq, ":gender", 1),
						(assign, ":snd", "snd_woman_yell"),
					(try_end),

		   			(agent_play_sound, ":winner_agent_no", ":snd"),
		   			(agent_play_sound, ":winner_agent_no", "snd_arena_ambiance"),  			
		   		(try_end),

	   			(assign, ":winner_x", ":x"),
	   			(assign, ":winner_y", ":y"),

			(else_try),
				(eq, ":i", ":loser_rectangle_index_2"),

				(assign, ":loser_rectangle_x_2", ":x"),
				(assign, ":loser_rectangle_y_2", ":y"),

			(else_try),
				(eq, ":i", ":loser_rectangle_index_1"),

				(store_sub, ":range_x", ":loser_rectangle_x_2", ":x"),
				(store_sub, ":range_y", ":loser_rectangle_y_2", ":y"),

				(try_for_agents, ":cur_agent_no"),
					(neq, ":cur_agent_no", ":winner_agent_no"),
					(agent_is_active, ":cur_agent_no"),
					(agent_is_alive, ":cur_agent_no"),
					(agent_is_human, ":cur_agent_no"),

					(call_script, "script_swordgame_agent_remove_all_items", ":cur_agent_no"),
					(agent_set_speed_modifier, ":cur_agent_no", 50),

					(store_random_in_range, ":add_x", 0, ":range_x"),
					(store_random_in_range, ":add_y", 0, ":range_y"),

					(init_position, pos2),
					(position_copy_origin, pos2, pos1),

					(position_move_x, pos2, ":add_x"),
					(position_move_y, pos2, ":add_y"),

					(store_add, ":cur_x", ":x", ":add_x"),
					(store_add, ":cur_y", ":y", ":add_y"),

					(store_sub, ":dX", ":winner_x", ":cur_x"),
					(store_sub, ":dY", ":winner_y", ":cur_y"),

					(store_atan2, ":rotZ", ":dX", ":dY"), #totally know what im doing!
					(store_sub, ":rotZ", 0, ":rotZ"),

					(position_rotate_z, pos2, ":rotZ"),

					(agent_set_position, ":cur_agent_no", pos2),
					(agent_set_animation, ":cur_agent_no", anim_wedding_guest_woman, 1),
	   				
					(try_begin),
						(call_script, "script_cf_swordgame_agent_is_male", ":cur_agent_no"),

						(store_random_in_range, ":rand", 0, 3),
						(try_begin),
							(eq, ":rand", 0),
							(assign, ":snd", "snd_man_yell"),
						(else_try),
							(eq, ":rand", 1),
							(assign, ":snd", "snd_man_victory"),
						(else_try),
							(eq, ":rand", 2),
							(assign, ":snd", "snd_encounter_vaegirs_ally"),
						(try_end),
			        (else_try),
			          (assign, ":snd", "snd_woman_yell"),
			        (try_end),

	   				(agent_play_sound, ":cur_agent_no", ":snd"),
				(try_end),

			(else_try),

				(set_spawn_position, pos1),
				(spawn_scene_prop, ":id"),
			(try_end),
		(try_end),
		
	]),
	





  	###########
  	### WSE ###
  	###########

		#script_wse_multiplayer_message_received
	# Called each time a composite multiplayer message is received
	# INPUT
	# script param 1 = sender player no
	# script param 2 = event no
	("wse_multiplayer_message_received", [
		(store_script_param, ":player_no", 1),
		(store_script_param, ":event_no", 2),
	]),

	#script_wse_game_saved
	# Called each time after game is saved successfully
	("wse_game_saved", [
	]),

	#script_wse_chat_message_received
	# Called each time a chat message is received (both for servers and clients)
	# INPUT
	# script param 1 = sender player no
	# script param 2 = chat type (0 = global, 1 = team)
	# s0 = message
	# OUTPUT
	# trigger result = anything non-zero suppresses default chat behavior. Server will not even broadcast messages to clients.
	# result string = changes message text for default chat behavior (if not suppressed).
	("wse_chat_message_received", [
		(store_script_param, ":player_no", 1),
		(store_script_param, ":chat_type", 2),
	]),

	#script_wse_console_command_received
	# Called each time a command is typed on the dedicated server console or received with RCON (after parsing standard commands)
	# INPUT
	# script param 1 = command type (0 - local, 1 - remote)
	# s0 = text
	# OUTPUT
	# trigger result = anything non-zero if the command succeeded
	# result string = message to display on success (if empty, default message will be used)
	("wse_console_command_received", [
		(store_script_param, ":command_type", 1),

		(try_begin),
			(str_starts_with, s0, "@swg "),

			(set_result_string, "@ "),
			(set_trigger_result, 1),

			(try_begin),
				(str_starts_with, s0, "@swg add list "),

				(str_store_substring, s0, s0, 13),
				(call_script, "script_swordgame_add_new_lvl_list"),

			(else_try),
				(str_starts_with, s0, "@swg add "),

				(str_store_substring, s0, s0, 8),
				(call_script, "script_swordgame_add_items_from_str_to_list"),

			(else_try),
				(str_starts_with, s0, "@swg set "),
				
				(str_store_substring, s0, s0, 8),
				(call_script, "script_swordgame_set_gvar"),

			(else_try),
				(str_equals, s0, "@swg list"),

				(display_message, "@----- Swordgame level list -----"),

				(array_create, ":tmp", 1, 0),
				(array_push, ":tmp", "@item 0"),
				(array_push, ":tmp", "@item 1"),
				(array_push, ":tmp", "@item 2"),
				(array_push, ":tmp", "@item 3"),
				(array_push, ":tmp", "@head  "),
				(array_push, ":tmp", "@body  "),
				(array_push, ":tmp", "@foot  "),
				(array_push, ":tmp", "@gloves"),
				(array_push, ":tmp", "@horse "),

				(array_get_dim_size, ":lvls", "$swordgame_cur_lvl_list", 0),

				(try_for_range, reg0, 0, ":lvls"),

					(display_message, "@-- lvl {reg0} --"),
					(try_for_range, ":j", 0, 9),
						(array_get_val, reg1, "$swordgame_cur_lvl_list", reg0, ":j"),
						(array_get_val, s0, ":tmp", ":j"),

						(display_message, "@{s0}: {reg1}"),
					(try_end),
				(try_end),

				(array_free, ":tmp"),
			(try_end),
		(try_end),

		(try_begin),
			(str_equals, s0, "@startA"),

			(array_create, "$swordgame_winner_stage_data", 0, 0, 7),

			(array_create, "$propsTmp", 0, 7),

			(assign, reg0, "$propsTmp"), #(display_message, "@{reg0}"),

		(else_try),
			(str_starts_with, s0, "@a "),
			(str_store_substring, s0, s0, 2),
			#(assign, reg0, "$propsTmp"), (display_message, "@{reg0}"),
			(str_split, ":n", s1, s0, "@ ", 1),

			(try_for_range, ":i", 1, 8),
				(str_store_string_reg, s0, ":i"),
				(str_to_num, ":num", s0),

				(val_sub, ":i", 1),
				(array_set_val, "$propsTmp", ":num", ":i"),
			(try_end),

			(try_for_range, ":i", 0, 7),
				(array_get_val, reg0, "$propsTmp", ":i"),
				#(display_message, "@{reg0}"),
			(try_end),

			(array_push, "$swordgame_winner_stage_data", "$propsTmp"),

		(else_try),
			(str_equals, s0, "@save"),
			(display_message, "@save!"),

			(array_load_file, "$swordgame_winner_stage_data", "@swordgameWinnerStageData"),

		(else_try),
			(str_equals, s0, "@rel"),

			(array_get_dim_size, ":n", "$swordgame_winner_stage_data", 0),

			(try_for_range, ":i", 1, ":n"),
				(try_for_range, ":j", 1, 7),
					(array_get_val, ":a", "$swordgame_winner_stage_data", 0, ":j"),
					(array_get_val, reg0, "$swordgame_winner_stage_data", ":i", ":j"),

					(val_sub, reg0, ":a"),
					#(display_message, "@{reg0}"),

					(array_set_val, "$swordgame_winner_stage_data", reg0, ":i", ":j"),
				(try_end),
			(try_end),

			(try_for_range, ":i", 1, 7),
				(array_set_val, "$swordgame_winner_stage_data", 0, 0, ":i"),
			(try_end),

		(else_try),
			(str_equals, s0, "@setupA"),

			(call_script, "script_swordgame_setup_win_stage"),

		(try_end),
	]),

	#script_wse_get_agent_scale
	# Called each time an agent is created
	# INPUT
	# script param 1 = troop no
	# script param 2 = horse item no
	# script param 3 = horse item modifier
	# script param 4 = player no
	# OUTPUT
	# trigger result = agent scale (fixed point)
	("wse_get_agent_scale", [
		(store_script_param, ":troop_no", 1),
		(store_script_param, ":horse_item_no", 2),
		(store_script_param, ":horse_item_modifier", 3),
		(store_script_param, ":player_no", 4),
	]),

	#script_wse_window_opened
	# Called each time a window (party/inventory/character) is opened
	# INPUT
	# script param 1 = window no
	# script param 2 = window param 1
	# script param 3 = window param 2
	# OUTPUT
	# trigger result = presentation that replaces the window (if not set or negative, window will open normally)
	("wse_window_opened", [
		(store_script_param, ":window_no", 1),
		(store_script_param, ":window_param_1", 2),
		(store_script_param, ":window_param_2", 3),
	]),

	#script_game_missile_dives_into_water
	# Called each time a missile dives into water
	# INPUT
	# script param 1 = missile item no
	# script param 2 = missile item modifier
	# script param 3 = launcher item no
	# script param 4 = launcher item modifier
	# script param 5 = shooter agent no
	# script param 6 = missile no
	# pos1 = water impact position and rotation
	("game_missile_dives_into_water", [
		(store_script_param, ":missile_item_no", 1),
		(store_script_param, ":missile_item_modifier", 2),
		(store_script_param, ":launcher_item_no", 3),
		(store_script_param, ":launcher_item_modifier", 4),
		(store_script_param, ":shooter_agent_no", 5),
		(store_script_param, ":missile_no", 6),
	]),

]